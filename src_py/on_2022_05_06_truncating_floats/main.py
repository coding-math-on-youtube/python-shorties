import timeit


def main():
    number: int = 10_000
    x: float = 7.8273241543515

    # rounds mathematically correct
    print(round(x, 2))  # the right ...

    # rounds mathematically correct
    print(float(f'{x:.2f}'))  # the good ...

    # does NOT round, just cut digits
    print(1.0e-2*(x//1.0e-2))  # the ugly ...

    # does NOT round, just cut digits
    print(1.0e-2*int(x*1.0e2))  # and the bad ...

    # modified and hence DOES now round mathematically correct
    print(1.0e-2*((x + 5.0e-3)//1.0e-2))  # as well as the other ugly ...

    # modified and hence DOES now round mathematically correct
    print(1.0e-2*int((x + 5.0e-3)*1.0e2))  # ... and the other bad

    # ======================================================================================================

    number: int = 10_000_000

    for method in ('round(x, 2)', "float(f'{x:.2f}')",
                   '1.0e-2*(x//1.0e-2)', '1.0e-2*int(x*1.0e2)',
                   '1.0e-2*((x + 5.0e-3)//1.0e-2)', '1.0e-2*int((x + 5.0e-3)*1.0e2)'):
        print(f"{method} takes {timeit.timeit(method, number = number, globals = locals())} sec")


if __name__ == '__main__': main()
